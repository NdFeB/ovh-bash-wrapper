#!/bin/bash

# Useful example:
# 
# appID="$(./ovh-bash-wrapper.sh /me/api/application)" # Retrieve application ID. Let's say you only have one, or else this line will fail
# for i in $(./ovh-bash-wrapper.sh /me/api/credential | sed -e 's/\[//' -e 's/\]//' | tr ',' ' '); do ./ovh-bash-wrapper.sh /me/api/credential/"$i"; done | grep "$appID"

#   -> You can retrieve creds and permissions for a particular app

# MISC NOTES:

# Convert a result in an associative array with jq "key" and "value"
# ./ovh-bash-wrapper.sh /domain/zone/ndfeb.fr/record/12345 | jq -S -r 'to_entries|map("\(.key)=\(.value)")|.[]'

# Sign request
# "$1$" + SHA1_HEX(AS+"+"+CK+"+"+METHOD+"+"+QUERY+"+"+BODY+"+"+TSTAMP)

#query="https://eu.api.ovh.com/1.0/me/api/application" # gives apps IDs
#query="https://eu.api.ovh.com/1.0/me/api/application/12345" # gives name, key, description of the app

############## FUNCTIONS ##############

# set -x

function show_usage() {
cat <<EOF
USAGE
    $0 [-h] [-v] [-n] [-i|--current-cred] [-c CONF_FILE] [-s SERVER] [-ak APP_KEY] [-as APP_SECRET] [-ck CONSUMER_KEY] [[-d BODY]...] [HTTP_METHOD] QUERY

OPTIONS
    -ak APP_KEY         Use APP_KEY as OVH Application Key
    -as APP_SECRET      Use APP_SECRET as OVH Application Secret
    -ck CONSUMER_KEY    Use CONSUMER_KEY as OVH Consumer Key
    -s, --server SERVER Use SERVER as OVH API submission server
    -d, --data BODY     Add BODY to the body of the API query. Must be x-www-form-urlencoded format (see curl)
                        Can be used multiple times
                        Example: -d 'key1=value1&key2=value2' or -d 'key1=value1' --data 'key2=value2'

    -i, --info          Get information about all credentials having access to the current application
                        This includes (for each app): Application Key due dates ; app details ; access rules for linked credentials
    --current-cred      Get information about current credential and linked application
                        This includes: Application Key due dates ;  application details ; access rules for current token

    -c, --config FILE   Use FILE as the configuration file
    -n, --no-new-line   Do not output new line after query result
    -v, --verbose       Be verbose, rich output
    -h, --help          Show this help and exit

ARGUMENTS
    HTTP_METHOD         Use the HTTP method HTTP_METHOD (GET,PUT,POST,DELETE). Default is GET
    QUERY               Send QUERY to the OVH API

EXAMPLES
    $0 -v -c /opt/mywrapper.conf /domain

    $0 GET /me

    $0 --data 'target=1.2.3.4' PUT /domain/zone/domain.tld/record/123456

EOF
}
function stderr() {
    (>&2 echo -e "$*")
}
function verbose() {
    (>&3 echo -e $*)
}
function check_error() {
    if [ ! -z "${err}" ]; then
        stderr "ERROR(S):\n"
        stderr "${err}"
        stderr "\n-h for help\n"
        exit 1
    fi
}
function finish() {
    exec 3>&- 9>&- 9<&- 2>/dev/null
}
#containsElement () {
#  local e match="$1"
#  shift
#  for e; do [ "$e" == "$match" ] && return 0; done
#  return 1
#}
function show_info() {
# Get appKey validity:
#       -> /me/api/application => array
#       -> Loop over result /me/api/application/${appID} -> find field applicationKey="$app_key" (now we have the appID linked to the key)
#       -> /me/api/credential => array
#       -> Loop over result /me/api/credential/${credID} => grep "\"applicationId\": ${appID}" => jq -S ''
#       -> In the result, there is the token expiration date, the appID and the access rules for the current token

# infoMode = 2 -> show info about current token and app using /auth/currentCredential
# infoMode = 1 -> show info about all credentials having access to current app

    # show info about current token and app using /auth/currentCredential and /me/api/application/$appID
    if [ ${infoMode} -eq 2 ]
    then
        declare -A cred
        local json_cred

        # Get current credential details
        query="/auth/currentCredential"
        json_cred=$(submit_query ${query})
        [ $? -ne 200 ] && stderr "Could not access '${query}':" && echo ${json_cred} | jq -S '' >&2 && return 1

        # Convert json_cred to a bash associative array
        while IFS="=" read -r key value
        do
            cred[$key]="$value"
        done < <(jq -S -r "to_entries|map(\"\(.key)=\(.value)\")|.[]" <<< "${json_cred}")

        # Get detail of the application linked to current credential
        query="/me/api/application/${cred[applicationId]}"

        echo 'Application linked to current credential:'
        submit_query "${query}" | jq -S ''

        echo 'Current credential:'
        echo "${json_cred}" | jq -S ''

    # show info about all credentials having access to current app
    elif [ ${infoMode} -eq 1 ]
    then
        local json_appIDs
        declare -A appIDs=()

        local json_creds_IDs
        declare -A credIDs=()

        local json_app
        declare -A app=() # consider this associative array like an "object" of type "application", with keys being attributes

        declare -a json_creds=()

        query="/me/api/application"
        json_appIDs="$(submit_query ${query})"
        [ $? -ne 200 ] && stderr "Could not access '${query}':" && echo ${json_appIDs} | jq -S '' && return 1

        # Convert json_appIDs to a bash array
        while IFS="=" read -r key value
        do
            appIDs[$key]="$value"
        done < <(jq -S -r "to_entries|map(\"\(.key)=\(.value)\")|.[]" <<< "${json_appIDs}")

        # Loop over /me/api/application/appIDs[@] and convert json results to bash arrays "app[]"
        # In each loop, in app[], we look for the key "applicationKey" with value equal to "$app_key"
        loop=0
        for appID in ${appIDs[@]}
        do
            ((loop++))
            echo -ne "\rLooking into application ${loop}/${#appIDs[@]}"

            query="/me/api/application/${appID}"
            json_app="$(submit_query ${query})"
            if [ $? -eq 200 ] # If the query fails, do not try to parse it
            then
                while IFS="=" read -r key value
                do
                    app[$key]="$value"
                done < <(jq -S -r "to_entries|map(\"\(.key)=\(.value)\")|.[]" <<< "${json_app}")
            fi
            # When we find the good application, we can fetch the credential(s) linked to it
            if [ "${app[""applicationKey""]}" == "${app_key}" ]
            then
                echo -e "\nCurrent application:"
                echo "${json_app}" | jq -S ''

                # Now, get all credentials linked to this appID, Then loop over it in cas there is more than one
                query='/me/api/credential/?applicationId='"${appID}"
                json_creds_IDs="$(submit_query ${query})"
                [ $? -ne 200 ] && stderr "Could not access '${query}':" && echo ${json_creds_IDs} | jq -S '' && return 1

                # We have all credentials in JSON format. Convert it to bash array
                while IFS="=" read -r key value
                do
                    credIDs[$key]="$value"
                done < <(jq -S -r "to_entries|map(\"\(.key)=\(.value)\")|.[]" <<< ${json_creds_IDs})

                # Now we loop over credentials, and fetch their attributes.
                # The attribute "applicationId" is always equal to the appID we fetched previously, e.g no need to compare [ "${cred_attr[""applicationId""]}" == "${app_attr[""applicationId""]}" ]
                # Just print the JSON output for each entry
                loop=0
                for credID in ${credIDs[@]}
                do
                    ((loop++))
                    echo -ne "\rFetching credential details ${loop}/${#credIDs[@]}"

                    query="/me/api/credential/${credID}"
                    json_creds+=("$(submit_query ${query})")
                done
                [ ${#json_creds[@]} -ne 0 ] && echo -e "\nCredential(s) linked to current token:"
                for i in ${json_creds[@]}
                do
                    echo "${i}" | jq -S ''
                done
            fi
        done
    fi
    return 0
}
function sign_query() {
    ts="$(curl -s https://api.ovh.com/1.0/auth/time 2>/dev/null)"
    signature='$1$'$(echo -n "${app_secret}"+"${consumer_key}"+"${method}"+"${full_query}"+"${body}"+"${ts}" | sha1sum | awk '{print $1}')
}
function submit_query() {
    [ $# -ne 1 ] && stderr "INTERNAL ERROR: wrong usage of funtion submit_query()" && exit 1
    full_query="${api_server}${1}"
    sign_query

    verbose "signature = ${signature}"

    # To be able to catch http error code and http content in different variables
    # we use anonymous pipes. Does not work on red-ony systems, but more reliable

    # Create a temporary named pipe
    pipe=$(mktemp -u)
    mkfifo ${pipe}

    # Attach it to file descriptor 9
    exec 9<>${pipe}
    # Unlink the named pipe
    rm ${pipe}

    # anything we write to fd 9 can be read back from it

    curl -s -w "\n%{http_code}\n" -X "${method}" \
    -H "Content-Type:application/x-www-form-urlencoded;charset=utf-8" \
    -H "X-Ovh-Application:${app_key}" \
    -H "X-Ovh-Timestamp:${ts}" \
    -H "X-Ovh-Signature:${signature}" \
    -H "X-Ovh-Consumer:${consumer_key}" \
    --data "${body}" "${full_query}" >&9

    read -r -u 9 httpResponse # HTTP response was the first in in the fifo
    read -r -u 9 LastHttpErrCode

    # Close the file descriptors
    exec 9>&- 9<&-

    verbose "LastHttpErrCode FROM WITHIN submit_query(): ${LastHttpErrCode}"

    # Now, output the http response and return last HTTP error code
    echo -n "${httpResponse}"
    return ${LastHttpErrCode}
}

##############################################################################
################################# MAIN BLOCK #################################
##############################################################################

###################### VARIABLES ##########################

err=""
verbose=0
newLine=1 # Add carriage return after query result
infoMode=0 # with -i or --info, infoMode = 1, with --current-cred, infoMode = 2. Show info about all apps/creds or just about current cred

# If script filename does not have an extension, do not try to truncate it
if [ -z "${0%.*}" ]; then
    conf_name="$(basename $0).conf"
else
    conf_name="$(basename ""${0%.*}"").conf"
fi

trap "finish" EXIT

#################### FETCH CONFIG ###################

# Try to find config file in default paths, taking user defined config over all
[ -f "/etc/${conf_name}" ]          && config_file="/etc/${conf_name}"
[ -f "$(dirname $0)/${conf_name}" ] && config_file="$(dirname $0)/${conf_name}"
[ -f "${HOME}/.${conf_name}" ]      && config_file="${HOME}/.${conf_name}"

# Can define config path with environment variable
[ ! -z "${OVH_BASH_WRAPPER_CONFIG}" ] && config_file="${OVH_BASH_WRAPPER_CONFIG}"

#################### ARG PARSING #####################

# Check if a config file was supplied by the user
# If yes, then source it before parsing any option, so it does not override custom values
i=1
while [ "$i" -le "$#" ]; do
    eval "arg=\${$i}"
    case ${arg} in
        -h|--help|\?)
            show_usage; exit 0 ;;
        -c|--config)
            # set $config_file to the value of next arg
            eval "config_file=\${$((i+1))}"
            [ ! -r "${config_file}" ] && err+="${config_file} does not exist or cannot be read\n"
            ;;
    esac
    i=$((i + 1))
done
check_error

# Now we can source the config file BEFORE parsing options
[ -r "${config_file}" ] && source "${config_file}" 2>/dev/null

positional=()
while [ $# -gt 0 ]
do
    arg="$1"
    case ${arg} in
        -h|--help|\?)       show_usage;         exit 0      ;; # already treated but meh
        -v|--verbose)       verbose=1;          shift       ;;
        -n|--no-new-line)   newLine=0;          shift       ;;
        -i|--info)          infoMode=1;         shift       ;; # info for all apps and linked credentials
        --current-cred)     infoMode=2;         shift       ;; # info for current credential and linked app
        -c|--config)                            shift; shift;; # ignore this
        -ak)                app_key="$2";       shift; shift;;
        -as)                app_secret="$2";    shift; shift;;
        -ck)                consumer_key="$2";  shift; shift;;
        -s|--server)        api_server="$2";    shift; shift;;
        -d|--data)          body+="$2"'&';      shift; shift;; # trailing '&' does not induce any error with x-www-form-urlencoded formated POST data
        *)                  positional+=("$1"); shift       ;;
    esac
done
set -- "${positional[@]}"

# Positional is either (HTTP_METHOD, QUERY) or (QUERY)

if [ "${verbose}" -eq 1 ]; then exec 3>/dev/stdout; else exec 3>/dev/null; fi

if [ "${#positional[@]}" -eq 1 ]; then
    query="${positional[0]}"
elif [ "${#positional[@]}" -gt 1 ]; then
    method="${positional[0]}"
    query="${positional[1]}"
fi

################################ ARG CHECK #############################

[ -z "${app_key}" ]      && err+="Missing Application Key\n"
[ -z "${app_secret}" ]   && err+="Missing Application Secret\n"
[ -z "${consumer_key}" ] && err+="Missing Consumer Key\n"

[ -z "${method}" ]       && method='GET'
[ "${method}" != "GET" ] && [ "${method}" != "POST" ]   && \
[ "${method}" != "PUT" ] && [ "${method}" != "DELETE" ] &&   err+="Wrong HTTP method type: ${method}\n"

[ -z "${api_server}" ]   && err+="Missing API server\n"

verbose "Config file        = ${config_file}"
verbose "Application Key    = ${app_key}"
verbose "Application Secret = ${app_secret}"
verbose "Consumer Key       = ${consumer_key}"

verbose "HTTP method        = ${method}"
verbose "API server         = ${api_server}"
verbose "API query          = ${query}"

check_error

if [ ${infoMode} -ne 0 ]
then
    show_info
else
    verbose "Submitting query..."
    submit_query "${query}"
    [ "${newLine}" -eq 1 ] && echo;
fi

exit 0
