# OVH Bash wrapper

Bash wrapper for OVH API

## Dependencies

If you use the -i or --info options, this script depends on the package jq, a "lightweight and flexible command-line JSON processor". It is used to convert JSON formatted strings to bash associative arrays.

This script is developed and tested on following environments:

- Distribution: debian 8.10 (jessie)
- Bash version: 4.3.30(1)-release (x86_64-pc-linux-gnu)
- jq version:   jq-1.4-1-e73951f

---

- Distribution: Ubuntu 16.04.5 LTS (running in WSL)
- Bash version: 4.3.48(1)-release (x86_64-pc-linux-gnu)
- jq version:   jq-1.5-1-a5b5cbe

Feel free to request a merge for a working environment that you've tested!
