#!/bin/bash

show_usage()
{
cat <<EOF
USAGE
    $0 [-h] [-v] [-c CONFIG]

OPTIONS
    -c, --config CONFIG Use CONFIG as configuration file
    -v, --verbose       Be verbose, rich output
    -h, --help          Show this help and exit
EOF
}

# Argument parsing

verbose=0
positional=()
while [[ $# -gt 0 ]]
    do
    key="$1"
    
    case $key in
        -v|--verbose)
            verbose=1
            shift
            ;;
        *)    # unknown option
            positional+=("$1") # save it in an array for later
            shift # past argument
            ;;
    esac
done
set -- "${positional[@]}" # restore positional parameters

[ $verbose -eq 0 ] && exec 1>/dev/null

exit 0
